<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth'], 
    function () {
    Route::post('/login', ['uses' => 'AuthController@login']);
    Route::post('/signup', ['uses' => 'AuthController@signup']);
    
    Route::group(['middleware' => 'auth:api'], 
    function() {
        Route::get('/logout', 'AuthController@logout');
        Route::get('/user', 'AuthController@user');
        Route::get('/users', 'AuthController@show');
        Route::get('/user/{id}', 'AuthController@show');
        Route::put('/user/update/{id}', 'AuthController@update');
    });
});

Route::get('user/{user_id}/', 'UserController@UserInfo')->middleware('api');
Route::get('/{NaamVereniging}/{GroepsNaam}/id', 'GroepController@groep')->middleware('api');
Route::get('/{NaamVereniging}/user', 'UserController@show')->middleware('api');


Route::get('{user_id}/subgroep/activiteiten', 'UserController@specificUser')->middleware('api');


Route::post('/postInleveren','InleverController@inleveren');
Route::post('/inleveren/update/goedkeuren','InleverController@goedkeuren');
Route::post('/inleveren/update/afkeuren','InleverController@afkeuren');
Route::get('/inleveren/download/{bestand}','InleverController@download');
Route::get('/ingeleverd', 'InleverController@nogTeBeoordelen');
Route::get('/inleveren','InleverController@show');

Route::get('/groep', 'GroepController@show')->middleware('api');
Route::post('/groep/create', 'GroepController@create')->middleware('api');
Route::put('/groep/users/delete', 'UserController@groepUserDelete')->middleware('api');
Route::get('/{NaamVereniging}/groep', 'VerenigingController@index')->middleware('api');
Route::get('{NaamVereniging}/groep/user/add', 'GroepController@GeenGroep')->middleware('api');
Route::get('{NaamVereniging}/{GroepsNaam}/users', 'GroepController@UsersInGroep')->middleware('api');


// Route::get('/groep/users', 'GroepController@groepen')->middleware('api');
Route::get('/laatstemededeling/{VerenigingNaam}', 'MededelingController@showLatest')->middleware('api');
Route::post('/groep/users', 'UserController@groep')->middleware('api');

// -------------------------------------Activiteit---------------------------------------------------------

Route::get('/activiteit', 'ActiviteitenController@show')->middleware('api');
Route::get('/pivotactiviteit', 'ActiviteitenController@showPivot')->middleware('api');
Route::get('/pivotactiviteit', 'ActiviteitenController@showPivot')->middleware('api');
Route::get('/activiteit/download/{bestand}', 'ActiviteitenController@download')->middleware('api');
Route::post('/uploadimg', 'ActiviteitenController@uploadImg')->middleware('api');
Route::post('/maakpivotactiviteit', 'ActiviteitenController@maakPivotActiviteit')->middleware('api');



// -------------------------------------Mededeling---------------------------------------------------------
Route::get('/mededeling', 'MededelingController@show')->middleware('api');
Route::get('/laatstemededeling', 'MededelingController@showLatest')->middleware('api');


Route::post('/maakmededeling', 'MededelingController@maakMededeling')->middleware('api');
Route::delete('/verwijderMededeling/{id}', 'MededelingController@verwijderMededeling')->middleware('api');


// -------------------------------------SubGroep---------------------------------------------------------
Route::get('{NaamVereniging}/{GroepsNaam}/{Subgroepsnaam}/delete', "SubGroepController@UsersSubGroep");
Route::get('/{NaamVereniging}/{GroepsNaam}/subgroep/add' ,'SubGroepController@GeenSubGroep');
Route::get('/{NaamVereniging}/{GroepsNaam}/subgroep', "SubGroepController@showSubgroepen");
Route::post('/subgroep/user/create', 'SubGroepController@AddUser')->middleware('api');
Route::put('/subgroep/users/delete', 'UserController@subgroepUserDelete')->middleware('api');
Route::get('/subgroep', 'SubGroepController@pivot')->middleware('api');

Route::get('/{NaamVereniging}/subgroep', "SubGroepController@ShowAll");
Route::get('/{NaamVereniging}/{GroepsNaam}/{Subgroepsnaam}', "SubGroepController@SubGroepIndex");

Route::post('/subgroep/create', 'SubGroepController@create')->middleware('api');
Route::get('/{GroepsNaam}/id', 'GroepController@groepid')->middleware('api');
// -------------------------------------Bug---------------------------------------------------------
Route::get('/{NaamVereniging}/verenigingid', 'VerenigingController@verenegingid');
