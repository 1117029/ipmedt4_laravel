<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user', 'UserController@show');
Route::get('/groep', 'GroepController@show');
Route::get('/subgroep', 'SubGroepController@show');

Route::get('/verenigingen', 'VerenigingController@show');


Route::view('/{path?}', 'index');
