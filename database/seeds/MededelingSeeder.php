<?php

use Illuminate\Database\Seeder;

class MededelingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mededeling')->insert([
            'Titel' => "Dit is de titel",
            'Beschrijving' => "Dit is een hele uitgebreide bschrijving voor de aankomende zeil wedstrijd.",
        ]);

        DB::table('mededeling')->insert([
            'Titel' => "Dit is de titel 2",
            'Beschrijving' => "Dit is een hele uitgebreide bschrijving voor de aankomende knoop wedstrijd.",
        ]);

        
        DB::table('mededeling')->insert([
            'Titel' => "Dit is de titel 3",
            'Beschrijving' => "Dit is een hele uitgebreide bschrijving voor de aankomende knoop wedstrijd.",
        ]);
    }
}
