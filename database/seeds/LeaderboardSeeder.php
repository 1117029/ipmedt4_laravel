<?php

use Illuminate\Database\Seeder;

class LeaderboardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leader_board')->insert([
            'SubgroepsNaam' => 'Okay',
            'ActiviteitNaam' => 'test1',
        ]);
    }
}
