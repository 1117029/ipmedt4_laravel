<?php

use Illuminate\Database\Seeder;

class GroepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groep')->insert([
            'Groepsnaam' => 'welpen',
            'vereniging_id' => 1,
            'Aantal leden' => 20,
        ]);

        DB::table('groep')->insert([
            'Groepsnaam' => 'andere welpen',
            'vereniging_id' => 2,
            'Aantal leden' => 20,
        ]);

        DB::table('groep')->insert([
            'Groepsnaam' => 'andere welpen',
            'vereniging_id' => 2,
            'Aantal leden' => 20,
        ]);

        DB::table('groep')->insert([
            'Groepsnaam' => 'andere welpen',
            'vereniging_id' => 1,
            'Aantal leden' => 20,
        ]);

        DB::table('groep')->insert([
            'Groepsnaam' => 'andere welpen',
            'vereniging_id' => 2,
            'Aantal leden' => 20,
        ]);

        DB::table('groep')->insert([
            'Groepsnaam' => 'andere welpen',
            'vereniging_id' => 2,
            'Aantal leden' => 20,
        ]);

        DB::table('groep')->insert([
            'Groepsnaam' => 'asdasd welpen',
            'vereniging_id' => 2,
            'Aantal leden' => 20,
        ]);
        
    }
}
