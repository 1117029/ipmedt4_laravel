<?php

use Illuminate\Database\Seeder;

class inleverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inleveren')->insert([
            'ActiviteitNaam' => 'Touw knopen',
            'tekst' => 'Ik heb een touw geknoopt met Jan',
            'rating' => '4'
        ]);

        DB::table('inleveren')->insert([
            'ActiviteitNaam' => 'Foto maken',
            'tekst' => 'Ik heb een foto gemaakt van Hogeschool Leiden',
            'rating' => '3'
        ]);

        DB::table('inleveren')->insert([
            'ActiviteitNaam' => 'Puzzel oplossen',
            'tekst' => 'We hebben samen met de welpen de puzzel opgelost.',
            'rating' => '5'
        ]);
    }
}
