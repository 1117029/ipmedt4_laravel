<?php

use Illuminate\Database\Seeder;

class SubgroepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subgroep')->insert([
            'SubGroepsnaam' => 'subgroep1',
            'groep_id' => 1,
            'Aantal leden' => 4,
            "Punten" => 0
        ]);
        DB::table('subgroep')->insert([
            'SubGroepsnaam' => 'subgroep1.0',
            'groep_id' => 3,
            'Aantal leden' => 5,
            "Punten" => 0
        ]);

        DB::table('subgroep')->insert([
            'SubGroepsnaam' => 'subgroep1.000',
            'groep_id' => 4,
            'Aantal leden' => 5,
            "Punten" => 0
        ]);

        DB::table('subgroep')->insert([
            'SubGroepsnaam' => '000',
            'groep_id' => 7,
            'Aantal leden' => 5,
            "Punten" => 0
        ]);
    }
}
