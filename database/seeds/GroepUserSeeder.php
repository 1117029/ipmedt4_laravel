<?php

use Illuminate\Database\Seeder;

class GroepUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groep_user')->insert([
            'groep_id' => 1,
            'user_id'=> 1,
        ]);
        
    }
}
