<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call([
            UserSeeder::class,
            SubgroepSeeder::class,
            ActiviteitSeeder::class,
            GroepSeeder::class,
            LeaderboardSeeder::class,
            VerenigingSeeder::class,
            
            // inleverSeeder::class,
            // GroepUserSeeder::class,
            //MededelingSeeder::class,

            ]);
    }
}
