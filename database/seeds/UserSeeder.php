<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'Naam' => 'Jeroen',
            'Achternaam'=> 'Rijsdijk',
            'Datum' => '1994-01-01',
            'Role' => 'admin',
            'Email' => 'a@a.com',
            'VerenigingNaam' => 'zeeverkenners',
            'password' => bcrypt(12345678),
        ]);
        DB::table('user')->insert([
            'Naam' => 'Jeroen',
            'Achternaam'=> 'de Meij',
            'Datum' => '2005-01-01',
            'Role' => 'lid',
            'Email' => 'b@b.com',
            'VerenigingNaam' => 'zeeverkenners',
            'password' => bcrypt(87654321),
        ]);

    }
}
