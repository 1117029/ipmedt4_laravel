<?php

use Illuminate\Database\Seeder;

class GroepVerenigingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groep_vereniging')->insert([
            'groep_id' => 1,
            'vereniging_id'=> 2,
        ]);
        DB::table('groep_vereniging')->insert([
            'groep_id' => 1,
            'vereniging_id'=> 1,
        ]);
    }
}
