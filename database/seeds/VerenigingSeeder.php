p<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class VerenigingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vereniging')->insert([
            'id'=> 1,
            'NaamVereniging' => 'zeeverkenners',
            'Vereniging_code' => Str::random(5)
        ]);

        DB::table('vereniging')->insert([
            'id'=> 2,
            'NaamVereniging' => 'scoutingverkenners',
            'Vereniging_code' => Str::random(5)
        ]);
    }
}
