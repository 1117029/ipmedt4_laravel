<?php

use Illuminate\Database\Seeder;

class UserGroepKoppelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groep_koppel')->insert([
            'Userid' => 1,
            'Groepsnaam' => 'welpen'
        ]);
    }
}
