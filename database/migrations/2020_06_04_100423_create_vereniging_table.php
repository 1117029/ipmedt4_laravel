<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerenigingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vereniging', function (Blueprint $table) {
            $table->id('id');
            $table->String("NaamVereniging")->unique();
            $table->String("Afbeelding")->nullable();
            $table->integer('Aantal leden')->default(0);
            $table->String("Vereniging_code", 5)->unique();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vereniging');
    }
}
