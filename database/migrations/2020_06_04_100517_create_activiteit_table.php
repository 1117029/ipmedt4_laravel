<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActiviteitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activiteit', function (Blueprint $table) {
            $table->id()->unique()->increments();
            $table->String('ActiviteitNaam');
            $table->String('Beschrijving');
            $table->String('Bijlage');
            $table->integer('Rating')->default(0);
            $table->String('Status')->default("nietAf");

            // $table->String('Subgroepsnaam')->references('Subgroepsnaam')->on('Subgroep');
            $table->integer('TeBehalenPunten')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activiteit', function(Blueprint $table){
            $table->dropForeign(['activiteit_Subgroepsnaam_foreign']);
        });
    }
}
