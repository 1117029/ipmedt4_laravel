<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->id('id')->unique();
            $table->String('Naam');
            $table->String('Achternaam');
            $table->date('Datum');
            $table->String('Role')->default('lid');
            $table->String('Email')->unique();
            $table->String('VerenigingNaam')->references('NaamVereniging')->on('Vereniging');
            $table->String('password');
            $table->rememberToken();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function(Blueprint $table){
            $table->dropForeign('user_NaamVereniging_foreign');
        });
    }
}
