<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubgroepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subgroep', function (Blueprint $table) {
            $table->id('id');
            $table->String('Subgroepsnaam')->unique();
            $table->String('groep_id')->references('id')->on('groep');
            $table->integer('Aantal leden')->nullable();
            $table->integer('Punten')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subgroep', function(Blueprint $table){
            $table->dropForeign(['subgroep_Groepsnaam_foreign']);
        });
    }
}
