<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInleverenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inleveren', function (Blueprint $table) {
            $table->String('ActiviteitNaam')->nullable();
            $table->integer('ActiviteitId')->nullable();

            // $table->String('Subgroepsnaam')->references('Subgroepsnaam')->on('Subgroep');
            $table->String('tekst');
            $table->String('bestand')->nullable();
            $table->String('rating');
            $table->String('Beoordelen')->default('nog te beoordelen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inleveren');
        Schema::table('activiteit', function(Blueprint $table){
            $table->dropForeign(['activiteit_Subgroepsnaam_foreign']);

        });
    }
}
