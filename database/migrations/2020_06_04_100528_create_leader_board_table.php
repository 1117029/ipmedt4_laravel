<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaderBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leader_board', function (Blueprint $table) {
            $table->String('SubgroepsNaam')->references('Subgroepsnaam')->on('Subgroep');
            $table->String('ActiviteitNaam')->references('ActiviteitNaam')->on('Activiteit');
            $table->integer('AantalPunten')->default(0);
            $table->String('Afbeelding')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_board');
        Schema::table('leader_board', function(Blueprint $table){
            $table->dropForeign('leader_board_Subgroepsnaam_forgein');
            $table->dropForeign('leader_board_ActiviteitNaam_forgein');
        });
    }
}
