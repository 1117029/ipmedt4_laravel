<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActiviteitSubgroepPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activiteit_subgroep', function (Blueprint $table) {
            $table->bigInteger('activiteit_id')->unsigned()->index();
            $table->foreign('activiteit_id')->references('id')->on('activiteit')->onDelete('cascade');
            $table->bigInteger('subgroep_id')->unsigned()->index();
            $table->foreign('subgroep_id')->references('id')->on('subgroep')->onDelete('cascade');
            $table->primary(['activiteit_id', 'subgroep_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activiteit_subgroep');
    }
}
