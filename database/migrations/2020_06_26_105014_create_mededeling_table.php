<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMededelingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mededeling', function (Blueprint $table) {
            $table->id()->unique()->increments();
            $table->timestamps();
            $table->String('Titel');
            $table->String('Beschrijving');
            $table->String('VerenigingNaam')->references('NaamVereniging')->on('Vereniging');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mededeling');
    }
}
