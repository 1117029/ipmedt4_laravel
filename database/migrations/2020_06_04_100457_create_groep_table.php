<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groep', function (Blueprint $table) {
            $table->id('id');
            $table->integer('vereniging_id')->references('id')->on('vereniging');
            $table->String('GroepsNaam');
            $table->String('Afbeelding')->nullable();
            $table->integer('Aantal leden')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('groep', function(Blueprint $table){
            $table->dropForeign(['groep_NaamVereniging_foreign']);
            
        });
    }
}
