<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Vereniging;
use App\Groep;

class VerenigingController extends Controller
{
    public function show(){
        return DB::table('vereniging')->get()->all();
    }

    public function verenegingid($vereniging){
        return Vereniging::select('id')->where('NaamVereniging','=',$vereniging)->get();
    }

    public function index($vereniging){
        return Vereniging::with('groep')->where('NaamVereniging','=',$vereniging)->get();
    } 

}
