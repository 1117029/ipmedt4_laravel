<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Activiteit;
use App\ActiviteitSubgroep;
use Storage;





class ActiviteitenController extends Controller
{
    public function show(){
        return DB::table('activiteit')->get()->all();
    }

    public function showPivot(){
        $activiteitPivot = Activiteit::with('subgroep')->get();
        return $activiteitPivot;
    }

    public function maakPivotActiviteit(Request $request){
        $subgroep_id = json_decode($request->subgroep_id);
        $activiteit_id = json_decode($request->activiteit_id);

        $activiteit_user = new ActiviteitSubgroep([
            'subgroep_id' => $subgroep_id,
            'activiteit_id' => $activiteit_id,
        ]);

        $activiteit_user->save();

        return response()->json([
            'message' =>  $activiteit_user
        ], 201);
    }

    public function download($bestand){
        $path = storage_path().'/'.'app'.'/public/'.$bestand;
        if (file_exists($path)) {
            return response()->download($path);
        }
    }


    public function uploadImg(Request $request){
        $img = $request->bestand;

        if(!is_array($img)){
            // error_log('Some message b;kdf.');
            $extension = explode('/', explode(':', substr($img, 0, strpos($img, ';')))[1])[1];
            $replace = substr($img, 0, strpos($img, ',')+1); 
            $image = str_replace($replace, '', $img);
            $image = str_replace(' ', '+', $image);
            $imageName = uniqid().'.'.$extension;
            Storage::disk('public')->put($imageName, base64_decode($image));

            $activiteit = new activiteit([
                'ActiviteitNaam' => $request->ActiviteitNaam,
                'Bijlage' => $imageName,
                'Beschrijving' => $request->beschrijving, 
                'TeBehalenPunten' => $request->TeBehalenPunten, 
            ]);
            $activiteit->save();
            return $activiteit;
        }

        $activiteit = new activiteit([
                'ActiviteitNaam' => $request->ActiviteitNaam,
                'Bijlage' => "empty",
                'Beschrijving' => $request->beschrijving, 
                'TeBehalenPunten' => $request->TeBehalenPunten, 
            ]);
        $activiteit->save();
        return $activiteit;
    }

    public function maakActiviteit(Request $request){

        $activiteit = new activiteit();
        error_log($request);

        $activiteit->ActiviteitNaam = $request->input('Activiteitnaam');
        $activiteit->Beschrijving = $request->input('beschrijving');
        // $activiteit->Bijlage = $request->file('Bijlage')->store('public');



        // $activiteit = Storage::putFile('upload', $request->file('Bijlage'));
        $activiteit->rating = $request->input('rating');
        // $activiteit->Subgroepsnaam = $request->input('Subgroepsnaam');
        $activiteit->TeBehalenPunten = $request->input('TeBehalenPunten');


        try{
            $activiteit->save();
            // return response()->json([
            //     'message' => "$activiteit"
            // ]);
        }
        catch(Exception $e){
            return response()->json([
                'message' => 'Activiteit aanmaken failed'
            ]);
        }
        return DB::table('activiteit')->get()->all();
    }

}
