<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Groep;
use App\Vereniging;
use App\Subgroep;
use App\SubGroepUser;
use DB;

class SubGroepController extends Controller
{
    public function show(){
        return DB::table('subgroep')->get()->all();
    }

    public function pivot(){
        $subgroep = Subgroep::with('users')->get();
        return $subgroep;
    }

    public function getSubGroepUser($id){
        $subgroep = User::with('subgroep')->get();

        return $subgroep;
    }


    public function index($subgroep){
        return Subgroep::where("SubGroepNaam", '=', $subgroep)->get();
    }

    
    public function SubGroepIndex($vereniging,$groep,$subgroep){
        $tree = Subgroep::whereHas('groep')->whereHas('groep', function($q) use($groep,$vereniging){
         return $q->where('GroepsNaam','=', $groep);
        })->whereHas('groep.vereniging', function($q) use($vereniging){
         $q->where('NaamVereniging','=',$vereniging);
     })->where("Subgroepsnaam", '=', $subgroep)->get();
        return $tree;
     }
  
    public function showSubgroepen($vereniging,$groep){
       $tree = Subgroep::whereHas('groep')->whereHas('groep', function($q) use($groep,$vereniging){
        return $q->where('GroepsNaam','=', $groep);
       })->whereHas('groep.vereniging', function($q) use($vereniging){
        $q->where('NaamVereniging','=',$vereniging);
    })->get();
       return $tree;
    }

    public function create(Request $request){
        $subgroep = new Subgroep([
            'Subgroepsnaam' => $request->Subgroepsnaam,
            'groep_id' => $request->groep_id,
        ]);

        $subgroep->save();

        return response()->json([
            'message' =>  $subgroep
        ], 201);
    }

    public function GeenSubGroep($vereniging,$groep){
        $tree = User::whereHas('groep')->whereHas('groep', function($q) use($groep,$vereniging){
         return $q->where('GroepsNaam','=', $groep);
        })->whereHas('groep.vereniging', function($q) use($vereniging){
         $q->where('NaamVereniging','=',$vereniging);
     })->doesntHave('subgroep')->get();
        return $tree;
     }

  
     public function UsersSubGroep($vereniging,$groep,$subgroep){
         $tree = User::whereHas('subgroep')->whereHas('subgroep', function($q) use($vereniging,$groep,$subgroep){
              $q->where('Subgroepsnaam', '=', $subgroep);
         })->whereHas('subgroep.groep', function($q) use($vereniging,$groep){
            $q->where('GroepsNaam','=', $groep);
         })->whereHas('subgroep.groep.vereniging',function($q) use($vereniging){
            $q->where('NaamVereniging','=',$vereniging);
         })->get();
         return $tree;
     }

     public function ShowAll($vereniging){
         $subgroep= Vereniging::with('subgroep')->where('NaamVereniging','=',$vereniging)->get();
         return $subgroep;
     }
    

    public function AddUser(Request $request){
        $subgroep_id = json_decode($request->subgroep_id);
        $user_id = json_decode($request->user_id);

        $subgroep_user = new SubGroepUser([
            'subgroep_id' => $subgroep_id,
            'user_id' => $user_id,
        ]);

        $subgroep_user->save();

        return response()->json([
            'message' =>  $subgroep_user
        ], 201);
    }
}
