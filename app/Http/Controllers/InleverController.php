<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
use Illuminate\Support\Str;
use App\Inleveren;
use App\Activiteit;


class InleverController extends Controller
{
    public function inleveren(Request $request)
    {
        $img = $request->bestand;

        if(!is_array($img)){
            $extension = explode('/', explode(':', substr($img, 0, strpos($img, ';')))[1])[1];
            $replace = substr($img, 0, strpos($img, ',')+1); 
            $image = str_replace($replace, '', $img);
            $image = str_replace(' ', '+', $image);
            $imageName = Str::random(10).'.'.$extension;
            Storage::disk('public')->put($imageName, base64_decode($image));
    
                $Inleveren = new Inleveren([
                    'ActiviteitNaam' => $request->ActiviteitNaam,
                    'ActiviteitId' => $request->ActiviteitId,
                    'tekst' => $request->tekst,
                    'bestand' => $imageName,
                    'rating' => $request->rating, 
                ]);
            $Inleveren->save();
            DB::table('activiteit')->where('id', $request->ActiviteitId,)->update(['Status' => 'nakijken']);
            return $imageName;
        }
        $Inleveren = new Inleveren([
            'ActiviteitNaam' => $request->ActiviteitNaam,
            'ActiviteitId' => $request->ActiviteitId,
            'tekst' => $request->tekst,
            'bestand' => "empty",
            'rating' => $request->rating, 
        ]);
        DB::table('activiteit')->where('id', $request->ActiviteitId,)->update(['Status' => 'nakijken']);
        $Inleveren->save();
        return $Inleveren;
    }
    
    public function goedkeuren(Request $request){

        error_log($request);

        DB::table('activiteit')->where('id', $request->ActiviteitId,)->update(['Status' => $request->NakijkStatus]);

        return inleveren::where('tekst', '=' ,$request->tekst)->update(['Beoordelen' => $request->beoordeeld]);
    }

    public function afkeuren(Request $request){
        DB::table('activiteit')->where('id', $request->ActiviteitId,)->update(['Status' => 'nietAf']);

        return inleveren::where('tekst', '=' ,$request->tekst)->update(['Beoordelen' => $request->beoordeeld]);
    }

    public function download($bestand){
        $path = storage_path().'/'.'app'.'/public/'.$bestand;
        if (file_exists($path)) {
            return response()->download($path);
        }
    }

    public function show(){
        return DB::table('inleveren')->get()->all();
    }

    public function nogTeBeoordelen(){
        return Inleveren::where('Beoordelen','=','nog te beoordelen')->get();
    }
}


