<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Mededeling;
use Carbon\Carbon;


class MededelingController extends Controller
{
    public function show(){
        return DB::table('mededeling')->get()->all();
    }

    public function showLatest($VerenigingNaam){ 
        return Mededeling::latest()->where('VerenigingNaam', '=', $VerenigingNaam)->first();
    }

    public function verwijderMededeling($mededelingId){ 
        $mededeling = Mededeling::where('id',"=",$mededelingId)->delete();
        
    }

    public function maakMededeling(Request $request){
        $mededeling = new mededeling();
        $mededeling->Titel = $request->input('Titel');
        $mededeling->Beschrijving = $request->input('Beschrijving');
        $mededeling->VerenigingNaam = $request->input('VerenigingNaam');

        try{
            $mededeling->save();
            // return response()->json([
            //     'message' => "$mededeling"
            // ]);
        }
        catch(Exception $e){
            return response()->json([
                'message' => 'mededeling aanmaken failed'
            ]);
        }
    }


}

