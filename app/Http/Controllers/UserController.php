<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Groep;
use App\GroepUser;
use App\Subgroep;
use App\Activiteit;

class UserController extends Controller
{
    public function show($vereniging){
        return User::where('VerenigingNaam','=',$vereniging)->get();
    }

    public function users(Groep $groep){
        $groep = Groep::with('User')->first();
        return $groep;
    }

    public function groep(Request $request){
        $groep_id = json_decode($request->groep_id);
        $user_id = json_decode($request->user_id);

        $groep_user = new GroepUser([
            'groep_id' => $groep_id,
            'user_id' => $user_id,
        ]);

        $groep_user->save();

        return response()->json([
            'message' =>  $groep_user
        ], 201);
    }

    public function groepUserDelete(Request $request){
        $user_id = json_decode($request->user_id);

        $user = User::find($user_id);
        $user->groep()->detach();

        return response()->json([
            'message' =>  $user
        ], 201);
    }

    public function subgroepUserDelete(Request $request){
        $user_id = json_decode($request->user_id);

        $user = User::find($user_id);
        $user->subgroep()->detach();

        return response()->json([
            'message' =>  $user
        ], 201);
    }

    public function specificUser($user){
        // $fiets = User::with('subgroep.activiteit',)->get();
        $fiets = User::with('subgroep.activiteit')->where('id','=',$user)->get();

        return $fiets;
    }
    public function UserInfo($user){
        return User::with('groep.subgroep')->where('id','=',$user)->get();
    }



}
