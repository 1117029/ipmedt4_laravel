<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Groep;
use App\User;
use App\Vereniging;

class GroepController extends Controller
{
    public function show(){
        return DB::table('groep')->get()->all();
    }

    public function create(Request $request){
        $groep = new Groep([
            'GroepsNaam' => $request->GroepsNaam,
            'vereniging_id' => $request->vereniging_id,
        ]);

        $groep->save();

        return response()->json([
            'message' =>  $groep
        ], 201);
    }

    // public function groepen(User $user){
    //     $user = User::with('groep')->first();
    //     return $user;
    // }

    public function GeenGroep(User $user, $vereniging){
        $user = User::doesntHave('groep')->where('VerenigingNaam','=',$vereniging)->get();
        return $user;
    }

    public function GroepUsers($groepUsers){
        return Groep::select('id')->where('GroepsNaam','=',$groepUsers)->get();
    } 

    public function groep($vereniging, $groep){
        $groep = Groep::where('GroepsNaam','=',$groep)->where('vereniging_id','=',$vereniging)->get();
        return $groep;
    }

    public function UsersInGroep($vereniging,$groep){
        $user = User::whereHas('groep', function($q) use($groep,$vereniging){
            $q->where('GroepsNaam',"=", $groep);
            $q->where('VerenigingNaam','=',$vereniging);
        })->get();
        return $user;
    }

}
  
