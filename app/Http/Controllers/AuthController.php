<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Vereniging;
use Hash;
use DB;
class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'Naam' => 'required|string',
            'Achternaam' => 'required|string',
            'Datum' => 'required',
            'Email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'Vereniging_code' => 'required|string'
        ]);

        $verenigingNaam = Vereniging::where('Vereniging_code', $request->Vereniging_code)->first();

        $user = new User([
            'Naam' => $request->Naam,
            'Achternaam' => $request->Achternaam,
            'Datum' => $request->Datum,
            'Email' => $request->Email,
            'password' => bcrypt($request->password),
            'VerenigingNaam' => $verenigingNaam->NaamVereniging
        ]);

        $user->save();

        return response()->json([
            'message' =>  $user
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'Email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['Email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => User::all()
            ], 401);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
    
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function update($id){

        User::where('id', '=' , $id)
        ->update(['Role' => 'admin']);
        error_log($User);
        
        return response()->json([
            'message' =>  $user
        ], 201);

        // return response()->json([
        //     'message' => 'Successfully updated'
        // ]);
    }
    public function show(){
        return DB::table('user')->get()->all();
    }
    public function userVereniging(Request $request)
    {
        $userVereniging = $request->user()->VerenigingNaam;
        $userVerenigingId =  Vereniging::select('id')->where('NaamVereniging',$userVereniging)->first();
        return $userVerenigingId;
    }
}