<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Activiteit extends Model
{
    public function subgroep(){
        return $this->belongsToMany('App\Subgroep');
    }
    public function users(){
        return $this->hasManyThrough('App\User', 'App\ActiviteitSubgroep');
    }


    protected $table = 'activiteit';

    protected $fillable = [
        'ActiviteitNaam',
        'Bijlage',
        'Beschrijving',
        'TeBehalenPunten',
    ];


    public $timestamps = false;
}
