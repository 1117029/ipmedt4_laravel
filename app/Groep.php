<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groep extends Model
{
    public function subgroep(){
        return $this->hasMany('App\Subgroep');
    }
    
    public function vereniging(){
        return $this->belongsTo('App\Vereniging');
    }

    public function User(){
        return $this->belongsToMany('App\User');
    }

    protected $table = 'groep'; 
    public $timestamps = false;

    protected $fillable = [
        'GroepsNaam', 'vereniging_id'
    ];
}
