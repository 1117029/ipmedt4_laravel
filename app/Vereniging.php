<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vereniging extends Model
{
    public function users(){
        return $this->hasMany('App\User');
    }
    // public function groep(){
    //     return $this->belongsToMany(Groep::class, 'groep_vereniging');
    // }
    public function groep(){
        return $this->hasMany('App\Groep');
    }

    public function subgroep(){
        return $this->hasManyThrough('App\Subgroep', 'App\Groep');
    }

    protected $table = 'vereniging';
    // protected $primaryKey = 'Vereniging_code';

}
