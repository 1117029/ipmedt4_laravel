<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inleveren extends Model
{
    public function subgroep(){
        return $this->belongsToMany('App\Subgroep');
    }

    public function activiteit(){
        return $this->belongsToMany('App\Activiteit');
    }

    protected $table = 'inleveren';

    protected $fillable = [
        'ActiviteitNaam',
        'ActiviteitId',
        'tekst',
        'bestand',
        'rating',
        'beoordeeld'
    ];

    public $timestamps = false;
}
