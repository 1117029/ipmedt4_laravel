<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroepUser extends Model
{
    protected $table = 'groep_user';
    public $timestamps = false;

    protected $fillable = [
        'groep_id', 'user_id'
    ];
}
