<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiviteitSubgroep extends Model
{
    protected $table = 'activiteit_subgroep'; 
    public $timestamps = false;

    protected $fillable = [
        'subgroep_id', 'activiteit_id'
    ];
}
