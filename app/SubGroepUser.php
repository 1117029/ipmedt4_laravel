<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubGroepUser extends Model
{
    protected $table = 'subgroep_user';
    public $timestamps = false;

    protected $fillable = [
        'subgroep_id', 'user_id'
    ];
}
