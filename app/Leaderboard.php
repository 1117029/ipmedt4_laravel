<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    public function groepen(){
        return $this->belongsToMany('App\Groep');
    }
}
