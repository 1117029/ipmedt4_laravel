<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subgroep extends Model
{
    
    public $timestamps = false;
    
    protected $table = 'subgroep';
    public function activiteit(){
        return $this->belongsToMany('App\Activiteit');
    }
    public function users(){
        return $this->belongsToMany('App\User');
    }
    public function groep(){
        return $this->belongsTo('App\Groep');
    }
    public function leaderboard(){
        return $this->hasOne('App\Leaderboard');
    }
    public function vereniging(){
        return $this->hasOneThrough('App\Vereniging', 'App\Groep');
    }

    protected $fillable = [
        'Subgroepsnaam', 'groep_id'
    ];
}
