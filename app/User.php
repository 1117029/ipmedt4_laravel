<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable 
{

    public function vereniging(){
        return $this->belongsTo('App\Vereniging');
    }
    public function groep(){
        return $this->belongsToMany('App\Groep');
    }
    public function subgroep(){
        return $this->belongsToMany('App\Subgroep');
    }

    public function activiteit(){
        return $this->hasManyThrough('App\Activiteit', 'App\Subgroep');
    }

    public $timestamps = false;
    
    protected $table = 'user';

    use Notifiable, HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Naam', 'Achternaam', 'Email', 'password', 'Datum', 'VerenigingNaam'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
